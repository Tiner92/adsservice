import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdDetailComponent } from './ad-detail/ad-detail.component';
import { AppMainComponent } from './appMain/appMain.component';

const routes: Routes = [
    { path: 'logout', redirectTo:'', pathMatch: 'full'},
    { path: '', component: AppMainComponent  },
    { path: ':id', component: AdDetailComponent },
    { path: 'edit/:id', component: AdDetailComponent},
    { path: 'delete/:id', component : AdDetailComponent},
    { path: 'edit', component : AdDetailComponent}    
  ];

  @NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  })
  export class AppRoutingModule {}