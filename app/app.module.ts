﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppComponent }  from './app.component';
import { PagerService, LoggedService, AdsList } from './_services/index';
import { HeaderComponent } from './header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { AdDetailComponent } from './ad-detail/ad-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { AppMainComponent } from './appMain/appMain.component';


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        LoginFormComponent,
        AdDetailComponent,
        AppMainComponent 
    ],
    providers: [
        PagerService,
        LoggedService,
        AdsList
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }