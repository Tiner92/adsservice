import { Component, OnInit} from '@angular/core';
import { Ad } from '../ad';
import {ActivatedRoute, Router} from '@angular/router';
import {AdsList, LoggedService} from '../_services/index';



@Component({
  moduleId: module.id,
  selector: 'app-ad-detail',
  templateUrl: './ad-detail.component.html',
  styleUrls: ['./ad-detail.component.css']
})

export class AdDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private adsList: AdsList, private loggedService: LoggedService) { }
  private ads: Ad[];
  ad: Ad;
  path: string;
  id: string;
  deleteView:  boolean = false;
  isAuthor: boolean = false;
  isEditor: boolean = false;
  logged: any;
  dataFromChild: any;
  
  ngOnInit(){
    this.logged = this.loggedService.getCurrUser();    
    this.path = this.route.snapshot.url[0].path;
    this.id = this.route.snapshot.paramMap.get('id');    
    this.adsList.getAdsList().then((data: Ad[])=>{            
      this.ads = data;
      if(this.path === 'edit' && this.id === this.path){
        // creation of an ad
        this.ad = new Ad;
        this.ad.title='';
        this.ad.descr='';
        this.isAuthor=true;        
      }else if(this.path === 'edit'){
        // editing of an add
        let tempId:number = parseInt(this.id);
        this.ad = this.ads.find(x => x.id === tempId);
        if(this.logged===this.ad.author){
          this.isAuthor=true;
        }        
      }else if(this.path === 'delete'){
        // removal of existing ad
        this.deleteView =true;
        let tempId:number = parseInt(this.id);
        this.ad = this.ads.find(x => x.id === tempId);        
        this.ads.splice(this.ads.indexOf(this.ad),1);
        let temp = JSON.stringify(this.ads);
        localStorage.setItem('adsList', temp);
        setTimeout(()=>{
          this.router.navigate(['']);
        },5000);          
      }else if(this.id === this.path){
        // simple visit of an ad
        let tempId:number = parseInt(this.id);        
        this.ad = this.ads.find(x => x.id === tempId);
        if(this.ad.author===this.logged){
          this.isEditor = true;
        }                
      }      
    });    
  }
  saveChanges(){
    if(this.ad.descr!='' && this.ad.title!=''){
      if(this.ad.id){
        let index: number =this.ads.indexOf(this.ad);
        this.ads.splice(index, 1, this.ad);      
      }else{
        this.ad.id = this.ads[(this.ads.length-1)].id + 1;
        this.ad.author = this.logged;
        this.ad.date = new Date();
        this.ad.date = this.ad.date.getFullYear()+'/'+(this.ad.date.getMonth()+1)+'/'+this.ad.date.getDate();
        this.ads.push(this.ad);           
      }
      let temp = JSON.stringify(this.ads);   
      localStorage.setItem('adsList', temp);
      this.isEditor = true;
      this.isAuthor = false;
      this.router.navigate([this.ad.id]);
    }
  }
  eventFromChild(data: any) {    
    this.dataFromChild = data;       
    this.ngOnInit();
    if(this.dataFromChild==="createAdd"){
      setTimeout(()=>{
        this.ad = new Ad;
        this.isAuthor=true;
        this.isEditor=false;
      },0);
    }
    this.logged = this.loggedService.getCurrUser();
    if(this.logged===this.ad.author){
      this.isEditor = true;      
    }else{
      this.isEditor = false;
    }
  } 
}
