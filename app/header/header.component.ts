import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  author: string;
  @Output() sendDataToParent = new EventEmitter<string>();
  constructor() {
    
  }
  dataFromChild: any;
  eventFromChild(data: any) {    
    this.dataFromChild = data;
    console.log(this.dataFromChild);
    this.sendDataToParent.emit(this.dataFromChild);
  }
  ngOnInit() {
  }

}
