import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {LoggedService} from '../_services/index';
import {User} from '../user';

@Component({
  moduleId: module.id,
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  user = new User();
  submitted = false;
  logged = false;
  passErr = 'Password is required';
  author: string;
  temp: any;
  @Output() sendDataToParent = new EventEmitter<string>(); 

  onSubmit() {
    var pass = localStorage.getItem(this.user.name)    
    if(pass!= null){
      if(pass === this.user.password){
        // proper login
        this.submitted = true;
        this.logged = true;
        this.author = this.user.name;
        this.passErr = 'Password is required';
        localStorage.setItem('currUser', this.user.name);
        this.sendDataToParent.emit('loggedIn');    
      }else{
        // wrong password
        this.user.password='';
        this.passErr = 'Password is incorrect, please try again';
      }      
    } else {
      // new user
      localStorage.setItem(this.user.name, this.user.password);
      this.submitted = true;
      this.logged = true;
      this.author = this.user.name;
      localStorage.setItem('currUser', this.user.name);
      this.sendDataToParent.emit('loggedIn');
    }
  }

  logOut(){    
    this.logged = this.submitted = false;
    this.author = this.user.name = this.user.password = '';
    this.passErr = 'Password is required';
    localStorage.setItem('currUser', '');
    this.sendDataToParent.emit('loggedOut'); 

  }
  
  createAd(){
    this.sendDataToParent.emit('createAdd');
  }

  constructor(private loggedService: LoggedService) { }

  ngOnInit() {
    this.temp = this.loggedService.getCurrUser();       
    if(typeof this.temp ==="string"){
      this.logged = true;
      this.author = this.user.name = this.temp;
      this.submitted = true;
    }
    
  }

}
