import { Component, OnInit} from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Ad} from '../ad';

import { PagerService, LoggedService, AdsList } from '../_services/index'

@Component({
    moduleId: module.id,
    selector: 'app-main',
    templateUrl: 'appMain.component.html',
    styleUrls: ['appMain.component.css']
})

export class AppMainComponent implements OnInit {
    constructor(private http: Http, private pagerService: PagerService, private loggedService: LoggedService, private adsList: AdsList) { }    
    selectedAd: Ad;   
    private allItems: Ad[];   
    name: any;
    dataFromChild: any;   
    pager: any = {};
    pagedItems: any[];

    ngOnInit() {
        this.adsList.getAdsList().then((data: Ad[])=>{            
            this.allItems = data;
            this.setPage(1);
            this.getLoggedUser();
        });
    }

    dataFromParent: any;
    eventFromParent(data:any){
        this.getLoggedUser();
    }
    
    getLoggedUser(){
        this.name = this.loggedService.getCurrUser();        
    }

    setPage(page: number) {
        // get pager object from service
        this.pager = this.pagerService.getPager(this.allItems.length, page);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

        
    eventFromChild(data: any) {    
        this.dataFromChild = data;
        console.log('received by app');
        console.log(this.dataFromChild);        
        this.ngOnInit();            
        this.getLoggedUser();
    }
}