﻿import { Component, OnInit, } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css']
})

export class AppComponent implements OnInit {  
    constructor(){}
    ngOnInit(){}    
    dataFromChild: any;      
    eventFromChild(data: any) {    
        this.dataFromChild = data;
        console.log('received by app');
        console.log(this.dataFromChild);             
    }    
}