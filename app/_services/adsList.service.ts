import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

@Injectable()

export class AdsList{
    constructor(private http: Http){}
    getAdsList(){
        return new Promise(resolve =>       
            this.http.get('./dummy-data.json')
                .map((response: Response) => response.json())
                .subscribe(data => {                            
                var temp = localStorage.getItem('adsList');
                var temp2: any;                             
                if(temp!='' && temp!= null){
                    temp2 = JSON.parse(temp);  
                    for(var i=0; i<data.length; i++){
                        let push = true;
                        for(var j=0; j<temp2.length; j++){                            
                            if(temp2[j].id===data[i].id){
                                push=false;
                            }
                            if(j===temp2.length-1 && push){
                                temp2.push(data[i]);
                            }
                        }
                    }
                }else{
                    temp2 = data;
                }                
                resolve(temp2);                
            })
        );
    }       
}